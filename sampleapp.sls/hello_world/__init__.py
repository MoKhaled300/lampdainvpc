"""Serverless Hello World function."""
import json
import requests
import os
import uuid

def handler(event, context):
    """Return Serverless Hello World."""
    body = {
        "message": "success sended to kibana",
        "input": event,
    }
     index = '/movies/_doc/'
    esdomain = os.getenv('DOMAIN')
    id = uuid.uuid1()
    
    esdomainurl = 'https://' + str(esdomain) + index + str(id)
    print('hereris env value',esdomainurl)
    headers = { "Content-Type": "application/json" }
    r = requests.post(esdomainurl, headers=headers, json=body)
    response = {"statusCode": 200, "body": r.text}
    return response
  